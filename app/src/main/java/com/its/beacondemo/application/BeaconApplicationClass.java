package com.its.beacondemo.application;

import android.app.Application;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.its.beacondemo.R;
import com.its.beacondemo.utilities.NotificationHandler;

import java.util.List;
import java.util.UUID;

/**
 * Created by eman.safwat on 9/6/2016.
 */
public class BeaconApplicationClass extends Application implements
        BeaconManager.ServiceReadyCallback, BeaconManager.MonitoringListener{

    public static String BEACON_IN_URL = "http://www.xe.com/currencyconverter/#rates";

    public static String BEACON_OUT_URL = "https://www.its.ws/Pages/contact-us.aspx";

    public static String BEACON_IN_MESSAGE = "Good morning Amr \n" +
            "Would you like to check currency rates?";

    public static String BEACON_OUT_MESSAGE = "Thanks for banking with us\n" +
            "Would you please evaluate the service?";

    public static String BEACON_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    public static int BEACON_MAJOR = 51266;
    public static int BEACON_MINOR = 61266;

    public static int BEACON_MAJOR2 = 51234;
    public static int BEACON_MINOR2 = 61234;

    private BeaconManager beaconManager;
    private Region definedRegion, definedRegion2;
    private Region firstBeaconRegion;
    private boolean beaconsDiscovered = false;

    @Override
    public void onCreate() {
        super.onCreate();

        setupBeaconManager();
    }

    public void setupBeaconManager() {

        beaconManager = new BeaconManager(getApplicationContext());

//        beaconManager.setRegionExitExpiration(30000);
//        beaconManager.setRangingListener(this);

        beaconManager.connect(this);

        beaconManager.setBackgroundScanPeriod(3000, 3000);
    }


    @Override
    public void onServiceReady() {

        beaconManager.setMonitoringListener(this);

        definedRegion = new Region("region1", UUID.fromString(BEACON_UUID), BEACON_MAJOR, BEACON_MINOR);
//        definedRegion2 = new Region("region2", UUID.fromString(BEACON_UUID), BEACON_MAJOR2, BEACON_MINOR2);

        beaconManager.startMonitoring(definedRegion);
//        beaconManager.startMonitoring(definedRegion2);
    }


    @Override
    public void onEnteredRegion(Region region, List<Beacon> list) {

        NotificationHandler.showNotification(this, "Beacon Demo",
                BEACON_IN_MESSAGE, BEACON_IN_URL);

    }

    @Override
    public void onExitedRegion(Region region) {

        NotificationHandler.showNotification(this, "Beacon Demo",
                BEACON_OUT_MESSAGE, BEACON_OUT_URL);
    }
}
