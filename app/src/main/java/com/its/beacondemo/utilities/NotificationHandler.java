package com.its.beacondemo.utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.its.beacondemo.MainActivity;

import java.util.Random;

/**
 * Created by Eman Safwat on 1/28/2016.
 */
public class NotificationHandler {

    public static void showNotification(Context context, String title, String message, String url) {

        Intent notifyIntent = new Intent(context, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Intent notificationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
//        notification.setLatestEventInfo(contexta, contentTitle, contentText, contentIntent);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context);

        Notification notification = builder.setContentIntent(pendingIntent)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setAutoCancel(true).setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setContentText(message).build();

        notification.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Random rand = new Random();

        int notificationIdentifier = rand.nextInt(1000000) + 1;

        notificationManager.notify(notificationIdentifier, notification);

    }
}
